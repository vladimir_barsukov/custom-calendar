package ru.vladimirbarsukov.customcalendar.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.vladimirbarsukov.customcalendar.Constants;
import ru.vladimirbarsukov.customcalendar.R;
import ru.vladimirbarsukov.customcalendar.adapter.CalendarAdapter;
import ru.vladimirbarsukov.customcalendar.util.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends Fragment {

    @BindView(R.id.month) TextView mMonthText;
    @BindView(R.id.custom_calendar) RecyclerView mCalendarView;

    public CalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        ButterKnife.bind(this, view);

        initCalendar();

        return view;
    }

    private void initCalendar() {
        Calendar calendar = Calendar.getInstance();
        mMonthText.setText(Utils.getMonthName(calendar.getTime()));
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int currentMonth = calendar.get(Calendar.MONTH);
        List<Date> dates = new ArrayList<>();

        int numberOfDaysFromWeekStart = Utils.calculate(calendar.getTime());
        if (numberOfDaysFromWeekStart > 0) {
            for (int i = 1; i < numberOfDaysFromWeekStart; i++) {
                dates.add(null);
            }
        }

        while (currentMonth == calendar.get(Calendar.MONTH)) {
            dates.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        CalendarAdapter adapter = new CalendarAdapter(getContext(), dates);
        mCalendarView.setLayoutManager(new GridLayoutManager(getContext(), Constants.DAYS_IN_WEEK));
        mCalendarView.setAdapter(adapter);
    }

}
