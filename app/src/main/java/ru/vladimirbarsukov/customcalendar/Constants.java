package ru.vladimirbarsukov.customcalendar;

public class Constants {

    public static final String HOLIDAYS_JSON = "{\n" +
            "  \"holidays\":[\"01.10.2017\", \"17.10.2017\"]\n" +
            "}";

    public static final int DAYS_IN_WEEK = 7;
}
