package ru.vladimirbarsukov.customcalendar.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ru.vladimirbarsukov.customcalendar.R;

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.ViewHolder> {

    private Context mContext;
    private List<Date> mDates;

    public CalendarAdapter(Context context, List<Date> dates) {
        this.mContext = context;
        this.mDates = dates;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.calendar_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setDateText(mDates.get(position));
    }

    @Override
    public int getItemCount() {
        return mDates.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mDateText;

        public ViewHolder(View itemView) {
            super(itemView);
            mDateText = (TextView) itemView.findViewById(R.id.calendar_day);
        }

        public void setDateText(Date date) {
            if (date == null)
                mDateText.setText("");
            else {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY)
                    mDateText.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));

                if (date.compareTo(Calendar.getInstance().getTime()) == 0) {
                    mDateText.setTypeface(null, Typeface.BOLD);
                    mDateText.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                }

                mDateText.setText(String.valueOf(dayOfMonth));
            }
        }
    }
}
