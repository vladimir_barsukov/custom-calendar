package ru.vladimirbarsukov.customcalendar.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Utils {

    public static String getMonthName(Date date) {
        return new SimpleDateFormat("LLLL").format(date);
    }

    public static int calculate(Date currentDate) {
        int numberOfDaysBeforeCurrent;
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DAY_OF_WEEK, -7);

        long diff = currentDate.getTime() - c.getTime().getTime();
        numberOfDaysBeforeCurrent = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

        return numberOfDaysBeforeCurrent;
    }

}
